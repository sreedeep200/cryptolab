## Resolving the other syntax tree nodes
That covers the interesting corners of the grammars. We handle every place
where a variable is declared, read or written, and every place where a scope is
created or destroyed. Even though they aren’t effected by variable resolution,
we also need visit methods for all of the other syntax tree nodes in order to
recurse into their subtrees.

## Interpreting Resolved Variables


Each time resolver visits a variable, it tells the
interpreter how many scopes there are between the current scope and the scope
where the variable is defined. At runtime, this corresponds exactly to the
number of environments between the current one and the enclosing one where
the interpreter can find the variable’s value. The resolver hands that number to
the interpreter by calling this:
void resolve(Expr expr, int depth) {
locals.put(expr, depth);
}

code uploaded

